<%@ page import="java.sql.*"%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>Data Mahasiswa</title>
  </head>
  <body>
    <div>
      <div><a href="SaveMahasiswa">Tambah Data</a></div>
      <table border="1">
        <tr>
          <td>No</td>
          <td>FullName</td>
          <td>Address</td>
          <td>Status</td>
          <td>Grade Physics</td>
          <td>Grade Calculus</td>
          <td>Grade Biologi</td>
          <td>Edit</td>
          <td>Delete</td>
        </tr>
            <% ResultSet r = (ResultSet) request.getAttribute("val");
            int i =1;
            %>

            <%while(r.next()){%>
            <tr>
                <td><%=i++%></td>
                <td><%=r.getString("fullname")%></td>
                <td><%=r.getString("address")%></td>
                <td><%=r.getString("status")%></td>
                <td><%=r.getString("gradesphysics")%></td>
                <td><%=r.getString("gradescalculus")%></td>
                <td><%=r.getString("gradesbiologi")%></td>
                <td><a href="EditMahasiswa?id=<%=r.getString("id")%>">edit</a></td>
                <td><form method="post" action="DeleteMahasiswa?id=<%=r.getString("id")%>"><button type="submit" onclick="return confirm('apakah anda yakin ingin menghapus data ini?')">delete</button</form></td>
            </tr>
            <%}%>

      </table>
    </div>
  </body>
</html>